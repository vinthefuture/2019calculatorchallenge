﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CalculatorApp.Tests
{
	[TestClass()]
	public class CalculatorTests
	{
		[TestMethod()]
		public void CalculatorTest()
		{
			Assert.IsTrue(true);
		}

		public string OutputString { get; set; }
		public List<int> ConvertedVals { get; set; }
		public List<string> Separators = new List<string> { ",", @"\n" };
		// \D - matches any non-digit character
		public const string regex1 = @"^//(?<delimeter>\D)\\n(?<numbers>.*)";
		public const string regex2 = @"^//((?<delimeter>\D)|\[(?<delimeter>\D+)\])\\n(?<numbers>.*)";
		public const string regex3 = @"^// ( (?<delimeter>\D){1} | (?<delimeters>(\[(?<delimeter>[^\[\]]+)\])+) )   \\n(?<numbers>.*)";
		//public const string regex4 = @"^// ( (?<delimeter>\D){1} | (?<delimeters>((?<operator>[-+\*/])?\[(?<delimeter>[^\[\]]+)\])+) )   \\n(?<numbers>.*)";

		public List<string> ProtectedParams = new List<string> { "-d", "-D", "-n", "-N", "-u", "-U" };
		public int UpperBound { get; set; }
		public bool DenyNegs { get; set; }

		[TestMethod()]
		public void ProcessStringEmptyTest()
		{
			var numList = "";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringOneGoodValTest()
		{
			var numList = "5";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("5 = 5"));
		}

		[TestMethod()]
		public void ProcessStringOneBadValTest()
		{
			var numList = "asdf";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringOneGoodValOneBadValTest()
		{
			var numList = "5,asdf";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("5+0 = 5"));
		}

		[TestMethod()]
		public void ProcessStringTwoGoodValsTest()
		{
			var numList = "1,1000";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("1+1000 = 1001"));
		}

		[TestMethod()]
		public void ProcessStringTwoBadValsTest()
		{
			var numList = "asdf,jkl;";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("0+0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringMoreThanTwoValsTest()
		{
			var numList = "5,10,15";
			ProcessString(numList);
			//Assert.IsTrue(OutputString.Equals("Please enter a maximum of 2 numbers."));
			Assert.IsTrue(OutputString.Equals("5+10+15 = 30"));
		}

		[TestMethod()]
		public void ProcessStringNewlineSeparatorTest()
		{
			var numList = @"5,10\n15";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("5+10+15 = 30"));
		}

		[TestMethod()]
		[ExpectedException(typeof(Exception))]
		public void ProcessStringDenyNegativesTest()
		{
			var numList = @"5,10,-15,-30";
			ProcessString(numList);

			Assert.IsTrue(OutputString.Equals("-15,-30"));
		}

		[TestMethod()]
		public void ProcessStringLessThanOneThousandTest()
		{
			var numList = @"5,10\n15,1001,30";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("5+10+15+0+30 = 60"));
		}

		[TestMethod()]
		public void ProcessStringSingleCharCustomDelimeterNoValsTest()
		{
			var numList = @"//;\n";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringSingleCharCustomDelimeterOneValTest()
		{
			var numList = @"//;\n2";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("2 = 2"));
		}

		[TestMethod()]
		public void ProcessStringSingleCharCustomDelimeterTwoValsTest()
		{
			var numList = @"//;\n2;5";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("2+5 = 7"));
		}

		[TestMethod()]
		public void ProcessStringSingleCharCustomDelimeterThreeValsTest()
		{
			var numList = @"//;\n2;5,asdf";
			ProcessString(numList);
			Assert.IsTrue(OutputString.Equals("2+5+0 = 7"));
		}

		[TestMethod()]
		public void ProcessStringSingleCharCustomDelimeterLongerThanOneCharTest()
		{
			var numList = @"//;;\n2,3";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("0+2+3 = 5"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharCustomDelimeterTest()
		{
			var numList = @"//[***]\n11***22***33";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("11+22+33 = 66"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharCustomDelimeterNoValsTest()
		{
			var numList = @"//[***]\n";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharCustomDelimeterAndCommasTest()
		{
			var numList = @"//[*]\n11*22,33";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("11+22+33 = 66"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharCustomDelimeterRepeatedTest()
		{
			var numList = @"//[*]\n11***22,33";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("11+0+0+22+33 = 66"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharMultipleCustomDelimeterNoValsTest()
		{
			var numList = @"//[***][;]\n";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharMultipleCustomDelimeterBadCombinationFallbackTest()
		{
			var numList = @"//[***];\n";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("0+0 = 0"));
		}

		[TestMethod()]
		public void ProcessStringMultiCharMultipleCustomDelimeterValsTest()
		{
			var numList = @"//[*][!!][r9r]\n11r9r22*33!!44";
			ProcessString(numList);
			// assume fallback to supporting previous formats
			// in this test \n and comma are separators
			Assert.IsTrue(OutputString.Equals("11+22+33+44 = 110"));
		}

		[TestMethod()]
		public void MakeNumberEmptyTest()
		{
			var strVal = "";
			var retVal = MakeNumber(strVal);
			Assert.IsTrue(retVal.Equals(0));
		}

		[TestMethod()]
		public void MakeNumberGoodPositiveValTest()
		{
			var strVal = "5";
			var retVal = MakeNumber(strVal);
			Assert.IsTrue(retVal.Equals(5));
		}

		[TestMethod()]
		public void MakeNumberGoodNegativeValTest()
		{
			var strVal = "-5";
			var retVal = MakeNumber(strVal);
			Assert.IsTrue(retVal.Equals(-5));
		}

		[TestMethod()]
		public void MakeNumberBadValTest()
		{
			var strVal = "asdf";
			var retVal = MakeNumber(strVal);
			Assert.IsTrue(retVal.Equals(0));
		}

		[TestMethod()]
		public void ProcessArgsUpperBoundTest()
		{
			string[] args = { "-u", "3000" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 3000);
		}

		[TestMethod()]
		public void ProcessArgsUpperBoundBadValTest()
		{
			string[] args = { "-u", "3000a" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 1000);
		}

		[TestMethod()]
		public void ProcessArgsUpperBoundWithOtherFlagsTest()
		{
			string[] args = { "-u", "3000", "-d" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 3000);
		}

		[TestMethod()]
		public void ProcessArgsDenyNegsTest()
		{
			string[] args = { "-u", "3000", "-n" };
			ProcessArgs(args);
			Assert.IsFalse(DenyNegs);
		}

		[TestMethod()]
		public void ProcessArgsMultiParams1Test()
		{
			string[] args = { "-u", "3000", "-n" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 3000);
			Assert.IsFalse(DenyNegs);
		}

		[TestMethod()]
		public void ProcessArgsMultiParams2Test()
		{
			string[] args = { "-u", "3000", "-d", "-n" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 3000);
			Assert.IsTrue(DenyNegs);
			Assert.IsTrue(Separators.Contains("-n"));
		}

		[TestMethod()]
		public void ProcessArgsMultiParams3Test()
		{
			string[] args = { "-u", "-n" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 1000);
			Assert.IsTrue(DenyNegs);
		}

		[TestMethod()]
		public void ProcessArgsMultiParams4Test()
		{
			string[] args = { "-u", "5000", "-N", "-d", "\\g" };
			ProcessArgs(args);
			Assert.IsTrue(UpperBound == 5000);
			Assert.IsFalse(DenyNegs);
			Assert.IsTrue(Separators.Contains("\\g"));
		}

		public void ProcessString(string numList)
		{
			// included for unit tests
			ConvertedVals = new List<int>();
			OutputString = "";
			UpperBound = 1000;
			DenyNegs = true;
			// end included for unit tests

			var numsToConvert = "";
			var allSeparators = new List<string>();

			var regex = new Regex(regex3, RegexOptions.IgnorePatternWhitespace);
			var matches = regex.Matches(numList);

			if (matches.Count.Equals(1)) //new pattern matching starting in req. 6
			{
				var match = matches[0];
				if (match.Groups["delimeters"].Success)
				{
					for (var ii = 0; ii < match.Groups["delimeter"].Captures.Count; ii++)
					{
						var customSeparator = match.Groups["delimeter"].Captures[ii].Value;
						allSeparators.Add(customSeparator);
					}
				}
				else
				{
					var customSeparator = match.Groups["delimeter"].Value;
					allSeparators.Add(customSeparator);
				}

				allSeparators.AddRange(Separators);
				numsToConvert = match.Groups["numbers"].Value;
			}
			else // original pattern matching
			{
				numsToConvert = numList;
				allSeparators.AddRange(Separators);
			}

			var strVals = numsToConvert.Split(allSeparators.ToArray(), StringSplitOptions.None);

			for (var ii = 0; ii < strVals.Length; ii++)
			{
				ConvertedVals.Add(MakeNumber(strVals[ii]));
			}

			if (DenyNegs)
			{
				OutputString = string.Join(",", ConvertedVals.Where(x => x < 0));
				if (!string.IsNullOrEmpty(OutputString))
				{
					throw new Exception(string.Format("Negative numbers denied: {0}", OutputString));
				}
			}

			OutputString = string.Format("{0} = {1}",
				string.Join("+", ConvertedVals),
				(long)ConvertedVals.Sum());

			ConvertedVals.Clear();
		}

		public int MakeNumber(string strVal)
		{
			// included for unit tests
			UpperBound = 1000;
			// end included for unit tests

			var retVal = 0;
			int.TryParse(strVal, out retVal);
			return retVal > UpperBound ? 0 : retVal;
		}

		public void ProcessArgs(string[] args)
		{
			// included for unit tests
			UpperBound = 1000;
			DenyNegs = true;
			// end included for unit tests

			int upperBound;

			for (var ii = args.Count() - 1; ii >= 0; ii--)
			{
				var param = args[ii];
				try
				{
					if (int.TryParse(param, out upperBound))
					{
						if (args[ii - 1].Equals("-u", StringComparison.OrdinalIgnoreCase))
						{
							UpperBound = upperBound;
							ii--;
						}
						else
						{
							//DisplayHelp();
						}
					}
					else if (args[ii].Equals("-n", StringComparison.OrdinalIgnoreCase))
					{
						if (ii == 0)
						{
							DenyNegs = false;
						}
						else if (args[ii - 1].Equals("-d", StringComparison.OrdinalIgnoreCase))
						{
							Separators.Add(args[ii]);
							ii--;
						}
						else if (args[ii - 1].Equals("-u", StringComparison.OrdinalIgnoreCase))
						{
							//DisplayHelp();
						}
						else
						{
							DenyNegs = false;
						}
					}
					else if (!ProtectedParams.Contains(args[ii]))
					{
						if (args[ii - 1].Equals("-d", StringComparison.OrdinalIgnoreCase))
						{
							Separators.Add(args[ii]);
							ii--;
						}
						else
						{
							//DisplayHelp();
						}
					}
				}
				catch
				{
					//DisplayHelp();
				}

			}
		}
	}
}