﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalculatorApp
{
	public class Calculator
	{
		public string OutputString { get; set; }
		public List<int> ConvertedVals { get; set; }
		public List<string> Separators { get; set; }
		// \D - matches any non-digit character
		public const string regex1 = @"^//(?<delimeter>\D)\\n(?<numbers>.*)";
		public const string regex2 = @"^//((?<delimeter>\D)|\[(?<delimeter>\D+)\])\\n(?<numbers>.*)";
		public const string regex3 = @"^// ( (?<delimeter>\D){1} | (?<delimeters>(\[(?<delimeter>[^\[\]]+)\])+) )   \\n(?<numbers>.*)";

		public List<string> ProtectedParams = new List<string> { "-d", "-D", "-n", "-N", "-u", "-U" };
		public int UpperBound { get; set; }
		public bool DenyNegs { get; set; }

		public Calculator()
		{
			ConvertedVals = new List<int>();
			OutputString = "";
			Separators = new List<string> { ",", @"\n" };
			UpperBound = 1000;
			DenyNegs = true;
		}

		/// <summary>
		/// Support an unlimited number of numbers e.g. 1,2,3,4,5,6,7,8,9,10,11,12 will return 78
		/// Support a newline character (literal) as an alternative delimiter e.g. 1\n2,3 will return 6
		/// Deny negative numbers. An exception should be thrown that includes all of the negative numbers provided
		/// Ignore any number greater than 1000 e.g. 2,1001,6 will return 8
		/// Support multiple delimiters of any length  
		///		use the format: //[{delimiter1}][{delimiter2}]...\n{numbers} e.g. //[*][!!][r9r]\n11r9r22*33!!44 will return 110
		///		all previous formats should also be supported
		/// </summary>
		/// <param name="numList"></param>
		public void ProcessString(string numList)
		{
			//ConvertedVals = new List<int>();

			var numsToConvert = "";
			var allSeparators = new List<string>();

			var regex = new Regex(regex3, RegexOptions.IgnorePatternWhitespace);
			var matches = regex.Matches(numList);

			if (matches.Count.Equals(1)) //new pattern matching starting in req. 6
			{
				var match = matches[0];
				if (match.Groups["delimeters"].Success)
				{
					for (var ii = 0; ii < match.Groups["delimeter"].Captures.Count; ii++)
					{
						var customSeparator = match.Groups["delimeter"].Captures[ii].Value;
						allSeparators.Add(customSeparator);
					}
				}
				else
				{
					var customSeparator = match.Groups["delimeter"].Value;
					allSeparators.Add(customSeparator);
				}

				allSeparators.AddRange(Separators);
				numsToConvert = match.Groups["numbers"].Value;
			}
			else // original pattern matching
			{
				numsToConvert = numList;
				allSeparators.AddRange(Separators);
			}

			var strVals = numsToConvert.Split(allSeparators.ToArray(), StringSplitOptions.None);

			for (var ii = 0; ii < strVals.Length; ii++)
			{
				ConvertedVals.Add(MakeNumber(strVals[ii]));
			}

			if (DenyNegs)
			{
				OutputString = string.Join(",", ConvertedVals.Where(x => x < 0));
				if (!string.IsNullOrEmpty(OutputString))
				{
					throw new Exception(string.Format("Negative numbers denied: {0}", OutputString));
				}
			}

			OutputString = string.Format("{0} = {1}",
				string.Join("+", ConvertedVals),
				(long)ConvertedVals.Sum());

			ConvertedVals.Clear();
		}

		public int MakeNumber(string strVal)
		{
			var retVal = 0;
			int.TryParse(strVal, out retVal);
			return retVal > UpperBound ? 0 : retVal;
		}
	}
}
