﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorApp
{
	public class Program
	{
		static bool runningFlag = true;
		public static Calculator Adder { get; set; }

		public static void Main(string[] args)
		{
			Adder = new Calculator();
			if (args.Count() > 0)
			{
				ProcessArgs(args);
			}
			while (runningFlag)
			{
				try
				{
					Adder.ProcessString(Console.ReadLine());
					Console.WriteLine(Adder.OutputString);
				}
				catch (Exception ex)
				{
					throw ex;
				}

			}
		}

		private static void ProcessArgs(string[] args)
		{
			if(args[0].Equals("/?"))
			{
				DisplayHelp();
				return;
			}

			int upperBound;

			for (var ii = args.Count() - 1; ii >= 0; ii--)
			{
				var param = args[ii];
				try
				{
					if (int.TryParse(param, out upperBound))
					{
						if (args[ii - 1].Equals("-u", StringComparison.OrdinalIgnoreCase))
						{
							Adder.UpperBound = upperBound;
							ii--;
						}
						else
						{
							DisplayHelp();
						}
					}
					else if (args[ii].Equals("-n", StringComparison.OrdinalIgnoreCase))
					{
						if (ii == 0)
						{
							Adder.DenyNegs = false;
						}
						else if (args[ii - 1].Equals("-d", StringComparison.OrdinalIgnoreCase))
						{
							Adder.Separators.Add(args[ii]);
							ii--;
						}
						else if (args[ii - 1].Equals("-u", StringComparison.OrdinalIgnoreCase))
						{
							DisplayHelp();
						}
						else
						{
							Adder.DenyNegs = false;
						}
					}
					else if (!Adder.ProtectedParams.Contains(args[ii]))
					{
						if (args[ii - 1].Equals("-d", StringComparison.OrdinalIgnoreCase))
						{
							Adder.Separators.Add(args[ii]);
							ii--;
						}
						else
						{
							DisplayHelp();
						}
					}
				}
				catch
				{
					DisplayHelp();
				}
			}

		}

		private static void DisplayHelp()
		{
			Console.WriteLine("CalculatorApp.exe help:");
			Console.WriteLine("CalculatorApp.exe -dD <delimeter> -nN -uU <int upper bound>");
			Console.WriteLine("    -d or -D: alternate delimeter");
			Console.WriteLine("");
			Console.WriteLine("    -n or -N: allow negative numbers. Default is deny.");
			Console.WriteLine("");
			Console.WriteLine("    -u or -U: integer upper bound. Default is 1000.");
			Console.WriteLine("");
			runningFlag = false;
		}
	}
}
